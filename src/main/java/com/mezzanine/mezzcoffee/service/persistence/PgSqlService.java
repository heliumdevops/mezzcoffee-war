package com.mezzanine.mezzcoffee.service.persistence;

import com.mezzanine.mezzcoffee.PersistenceException;
import com.mezzanine.mezzcoffee.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author Torbjørn Kristoffersen <tk@mezzanineware.com>
 */
public class PgSqlService implements PersistenceService {

    private final DataSource dataSource;

    private static final Logger LOGGER = Logger.getLogger(PgSqlService.class.getName());
    private static final String TABLE_COUNT = "select count(*) from pg_catalog.pg_class where relname = ?";
    private static final String CREATE_COFFEE_LOG_TABLE = "create table coffee_log ( "
            + "  id serial, "
            + "  event_type varchar(25) not null, "
            + "  tstamp timestamp not null default now() "
            + ")";
    private static final String CREATE_BREW_HISTORY_VIEW = "create view brew_history as "
            + "SELECT tstamp as event_time, event_type, "
            + "tstamp - lag(tstamp) OVER (order by tstamp) as time_diff FROM coffee_log "
            + "where coffee_log.tstamp > '2014-07-15'";
    private static final String INSERT_COFFEE_LOG_EVENT = "insert into coffee_log (event_type) values (?)";
    private static final String GET_BREW_TIME_AVG = "select extract(epoch from avg(time_diff)) from "
            + "brew_history where event_type='READY'";
    private static final String GET_TOTAL_BREWS_TODAY = "select count(*) from brew_history where event_type='READY' and event_time > current_date::timestamp and time_diff > INTERVAL '3 minute'";
    private static final String GET_TOTAL_BREWS_THIS_MONTH = "select count(*) from brew_history where event_type='READY' and event_time > date_trunc('month', current_date) and time_diff > INTERVAL '3 minute'";
    private static final String GET_TOTAL_BREWS_ALL_TIME = "select count(*) from brew_history where event_type='READY' and time_diff > INTERVAL '3 minute'";
    private static final String GET_TOTAL_BREWS_15_MINS = "select count(*) from brew_history where event_type='READY' and event_time > now()- INTERVAL '15 minutes' and time_diff > INTERVAL '3 minute'";

    public PgSqlService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private void createCoffeeLogTable(final Connection connection) throws SQLException {
        try (final PreparedStatement stmt = connection.prepareStatement(CREATE_COFFEE_LOG_TABLE)) {
            stmt.executeUpdate();
        }
    }

    private void createBrewHistoryTable(final Connection connection) throws SQLException {
        try (final PreparedStatement stmt = connection.prepareStatement(CREATE_BREW_HISTORY_VIEW)) {
            stmt.executeUpdate();
        }
    }

    @Override
    public void updateSchema() {
        try (final Connection connection = dataSource.getConnection();
                final PreparedStatement stmt = connection.prepareStatement(TABLE_COUNT)) {
            stmt.setString(1, "coffee_log");
            try (ResultSet resultSet = stmt.executeQuery()) {
                if (resultSet.next()) {
                    Long tableCount = resultSet.getLong(1);
                    if (tableCount == 0) {
                        createCoffeeLogTable(connection);
                    }
                }
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.FINE, null, ex);
            throw new PersistenceException(ex);
        }

        try (final Connection connection = dataSource.getConnection();
                final PreparedStatement stmt = connection.prepareStatement(TABLE_COUNT)) {
            stmt.setString(1, "brew_history");
            try (ResultSet resultSet = stmt.executeQuery()) {
                if (resultSet.next()) {
                    Long tableCount = resultSet.getLong(1);
                    if (tableCount == 0) {
                        createBrewHistoryTable(connection);
                    }
                }
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.FINE, null, ex);
            throw new PersistenceException(ex);
        }
    }

    @Override
    public void insertCoffeeLogEvent(Util.BrewStatus brewStatus, Date tstamp) {
        try (final Connection connection = dataSource.getConnection();
                final PreparedStatement stmt = connection.prepareStatement(INSERT_COFFEE_LOG_EVENT)) {
            stmt.setString(1, brewStatus.name());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.log(Level.FINE, null, ex);
            throw new PersistenceException(ex);
        }
    }

    @Override
    public double getAverageBrewingTime() {

        try (final Connection connection = dataSource.getConnection();
                final Statement stmt = connection.createStatement();
                final ResultSet resultSet = stmt.executeQuery(GET_BREW_TIME_AVG)) {

            if (!resultSet.next()) {
                return -1;
            }
            return resultSet.getDouble(1);
        } catch (SQLException ex) {
            LOGGER.log(Level.FINE, null, ex);
            throw new PersistenceException(ex);
        }
    }

    @Override
    public int getDailyBrewCount() {
        try (final Connection connection = dataSource.getConnection();
                final Statement stmt = connection.createStatement();
                final ResultSet resultSet = stmt.executeQuery(GET_TOTAL_BREWS_TODAY)) {

            if (!resultSet.next()) {
                return -1;
            }

            return resultSet.getInt(1);
        } catch (SQLException ex) {
            LOGGER.log(Level.FINE, null, ex);
            throw new PersistenceException(ex);
        }
    }

    @Override
    public int getMonthlyBrewCount() {
        try (final Connection connection = dataSource.getConnection();
                final Statement stmt = connection.createStatement();
                final ResultSet resultSet = stmt.executeQuery(GET_TOTAL_BREWS_THIS_MONTH)) {

            if (!resultSet.next()) {
                return -1;
            }

            return resultSet.getInt(1);
        } catch (SQLException ex) {
            LOGGER.log(Level.FINE, null, ex);
            throw new PersistenceException(ex);
        }
    }

    @Override
    public int getAllTimeBrewCount() {
        try (final Connection connection = dataSource.getConnection();
                final Statement stmt = connection.createStatement();
                final ResultSet resultSet = stmt.executeQuery(GET_TOTAL_BREWS_ALL_TIME)) {

            if (!resultSet.next()) {
                return -1;
            }

            return resultSet.getInt(1);
        } catch (SQLException ex) {
            LOGGER.log(Level.FINE, null, ex);
            throw new PersistenceException(ex);
        }
    }

    @Override
    public int getLast15MinutesBrewCount() {
        try (final Connection connection = dataSource.getConnection();
                final Statement stmt = connection.createStatement();
                final ResultSet resultSet = stmt.executeQuery(GET_TOTAL_BREWS_15_MINS)) {

            if (!resultSet.next()) {
                return -1;
            }

            return resultSet.getInt(1);
        } catch (SQLException ex) {
            LOGGER.log(Level.FINE, null, ex);
            throw new PersistenceException(ex);
        }
    }

}
