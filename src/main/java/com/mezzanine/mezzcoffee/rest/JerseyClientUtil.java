package com.mezzanine.mezzcoffee.rest;

import java.net.SocketTimeoutException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 *
 * @author jev
 */
public class JerseyClientUtil {

    private final static int MAX_RETRIES = 1;

    public static String getCurlPattern(final String username, final String password, final String uri, final String method) {
        return getCurlPattern(username, password, uri, method, null);
    }

    public static String getCurlPattern(final String username, final String password, final String uri, final String method, final String dataParam) {
        final String data = dataParam != null ? String.format("-d '%s'", dataParam) : "";
        return String.format("curl -v -u '%s:%s' -X %s '%s' %s | python -mjson.tool", username, password, method, uri, data);
    }

    public static boolean isValidResponse(final int status) {
        return status == 200 || status == 204;
    }

    /**
     * Process Web Target up to Constants.MAX_RETRIES times when 502 or
     * SocketTimeoutException occurs or until getValidTypes valid response is
     * received from the server
     *
     * @param target
     * @param method
     * @param entity
     * @param mediaType
     * @param username
     * @param password
     * @return Response or empty if unable to get getValidTypes response on all
     * tries
     */
    public static Optional<Response> processWebTargetWithRetry(
            final WebTarget target,
            final String method,
            final Entity<?> entity,
            final String mediaType,
            final String username,
            final String password) {
        final Logger LOG = Logger.getLogger(JerseyClientUtil.class.getName());
        final String curl = getCurlPattern(username, password, target.getUri().toString(), method);
        for (int i = 1; i <= MAX_RETRIES; i++) {

            final String retryString = String.format("Process WebTarget re-try count is %s of %s", i, MAX_RETRIES);
            try {

                final Response response;

                final Invocation.Builder builder = target
                        .request()
                        .accept(mediaType);

                if (method.equals(HttpMethod.POST)) {
                    response = builder.post(entity);
                } else {
                    response = builder.get(Response.class);
                }

                if (response.getStatus() == Response.Status.BAD_GATEWAY.getStatusCode()) {
                    final String msg = String.format("Bad Gateway (%s) response from %s, %s :: %s",
                            response.getStatus(), target.getUri().toString(), retryString, curl);
                    LOG.log(Level.SEVERE, msg);
                    continue;
                }

                return Optional.of(response);
            } catch (final ProcessingException e) {
                if (e.getCause() instanceof SocketTimeoutException) {
                    final String msg = String.format("Socket Timeout Exception from %s, %s :: %s",
                            target.getUri().toString(), retryString, curl);
                    LOG.log(Level.SEVERE, msg, e);
                } else {
                    throw e;
                }
            }
        }
        final String msg = String.format("Consecutively Failed to Process %s %s times in a row. %s",
                target.getUri().toString(), MAX_RETRIES, curl);
        LOG.log(Level.SEVERE, msg);
        return Optional.empty();
    }
}
