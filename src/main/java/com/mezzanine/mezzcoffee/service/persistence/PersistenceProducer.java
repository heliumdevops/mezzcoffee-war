package com.mezzanine.mezzcoffee.service.persistence;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.sql.DataSource;

/**
 *
 * @author Torbjørn Kristoffersen <tk@mezzanineware.com>
 */
@ApplicationScoped
public class PersistenceProducer {

    @Resource(mappedName = "jdbc/MezzCoffee")
    private DataSource dataSource;
    
    @Produces
    public PersistenceService getIntegrationService() {
        return new PgSqlService(dataSource);
    }
}
