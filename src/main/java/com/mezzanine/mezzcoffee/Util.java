package com.mezzanine.mezzcoffee;

/**
 *
 * @author Torbjørn Kristoffersen <tk@mezzanineware.com>
 */
public class Util {

    public static enum BrewStatus {

        IDLE,
        BREWING,
        READY
    };

    public static String ordinal(int number) {
        final String[] suffixes = new String[]{"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
        switch (number % 100) {
            case 11:
            case 12:
            case 13:
                return number + "th";
            default:
                return number + suffixes[number % 10];
        }
    }
}
