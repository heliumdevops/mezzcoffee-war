package com.mezzanine.mezzcoffee;

import javax.ejb.ApplicationException;

/**
 *
 * @author Torbjørn Kristoffersen <tk@mezzanineware.com>
 */
@ApplicationException(rollback = true)
public class PersistenceException extends RuntimeException {

    public PersistenceException(String message) {
        super(message);
    }
    
    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistenceException(Throwable cause) {
        super(cause);
    }
}
