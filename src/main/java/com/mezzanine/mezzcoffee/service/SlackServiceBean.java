package com.mezzanine.mezzcoffee.service;

import com.mezzanine.mezzcoffee.rest.JerseyClientUtil;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.mezzanine.mezzcoffee.rest.JerseyClientUtil.isValidResponse;
import static javax.ws.rs.client.Entity.entity;

/**
 *
 * @author jev
 */
@RequestScoped
public class SlackServiceBean {

    private static final String API_URL = "https://hooks.slack.com/services/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";
    private static final String CHANNEL = "#coffee";
    private static final String USERNAME = "mezzcoffee";

    private static final Client CLIENT = ClientBuilder.newClient();
    private static final Logger LOG = Logger.getLogger(SlackServiceBean.class.getName());

    public void sendMessage(final String msg) {
        sendMessageToRoom(msg, CHANNEL, USERNAME);
    }

    private void sendMessageToRoom(final String msg, final String channel, final String username) {

        try {
            final WebTarget target = CLIENT.target(API_URL);

            final String json = String.format("{\"text\": \"%s\", \"channel\": \"%s\", \"link_names\": 1, \"username\": \"%s\", \"icon_emoji\": \":coffee:\"}", msg, channel, username);
            final Entity<?> entity = entity(json, MediaType.APPLICATION_JSON);

            final Optional<Response> response = JerseyClientUtil.processWebTargetWithRetry(target, HttpMethod.POST, entity, MediaType.APPLICATION_JSON, "", "");

            if (!response.isPresent()) {
                throw new Exception("Unable to get response from Slack");
            }

            if (!isValidResponse(response.get().getStatus())) {
                throw new Exception("Bad response from Slack");
            }

            LOG.log(Level.FINE, "Sent this message to Slack: {0}", msg);

        } catch (Exception ex) {
            LOG.log(Level.WARNING, "Could not send Slack notification", ex);
        }
    }
}
