package com.mezzanine.mezzcoffee.service;

import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.mezzanine.mezzcoffee.Util.BrewStatus;
import com.mezzanine.mezzcoffee.service.persistence.PersistenceService;

/**
 *
 * @author Torbjørn Kristoffersen <tk@mezzanineware.com>
 */
@ApplicationScoped
public class CoffeeServiceBean {

    private static final String COFFEE_IS_BREWING = "Coffee is brewing...";
    private static final String COFFEE_IS_READY = "Coffee brew number daily_count is ready!";

    private Date lastUpdated = new Date();
    private BrewStatus status = BrewStatus.IDLE;

    @Inject
    private PersistenceService persistenceService;

    @Inject
    private SlackServiceBean slackServiceBean;

    public String getBrewingStatus() {
        return status.name();
    }

    public void notifyDailyKillStreak(final int dailyBrewCount) {

        String dailyStreakText = null;

        if (dailyBrewCount <= 2) {
            return;
        }

        if (dailyBrewCount == 3) {
            dailyStreakText = "MezCoffee is on a Killing Spree!";
        } else if (dailyBrewCount == 4) {
            dailyStreakText = "MezCoffee is Dominating!";
        } else if (dailyBrewCount == 5) {
            dailyStreakText = "MezCoffee has a Mega Kill!";
        } else if (dailyBrewCount == 6) {
            dailyStreakText = "MezCoffee is Unstoppable!";
        } else if (dailyBrewCount == 7) {
            dailyStreakText = "MezCoffee is Wicked Sick!";
        } else if (dailyBrewCount == 8) {
            dailyStreakText = "MezCoffee has a Monster kill!";
        } else if (dailyBrewCount == 9) {
            dailyStreakText = "MezCoffee is Godlike!";
        } else if (dailyBrewCount >= 10) {
            dailyStreakText = "MezCoffee is Beyond Godlike!";
        }

        if (dailyBrewCount >= 3 && dailyStreakText != null) {
            slackServiceBean.sendMessage(dailyStreakText);
        }

    }

    public void notifyMultiKill() {
        int multiKillCount = getLast15MinutesBrewCount();
        String multiKillText = null;

        switch (multiKillCount) {
            case 2:
                multiKillText = "Double kill!";
                break;
            case 3:
                multiKillText = "Triple kill!";
                break;
            case 4:
                multiKillText = "Ultra kill!";
                break;
            case 5:
                multiKillText = "Rampage!!";
                break;
            case 6:
                multiKillText = "Double Rampage!!";
                break;
            case 7:
                multiKillText = "Triple Rampage!!";
                break;
            default:
                break;
        }

        if (multiKillText != null) {
            slackServiceBean.sendMessage(multiKillText);
        }

    }

    public void setBrewingStatus(BrewStatus brewingStatus) {
        this.status = brewingStatus;

        this.lastUpdated = new Date();
        persistenceService.insertCoffeeLogEvent(brewingStatus, this.lastUpdated);

        if (brewingStatus.equals(BrewStatus.READY)) {
            int dailyBrewCount = getDailyBrewCount();
            slackServiceBean.sendMessage(COFFEE_IS_READY.replaceAll("daily_count", String.valueOf(dailyBrewCount)));
            notifyDailyKillStreak(dailyBrewCount);
            notifyMultiKill();
        } else if (brewingStatus.equals(BrewStatus.BREWING)) {
            slackServiceBean.sendMessage(COFFEE_IS_BREWING);
        }

    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public int getMonthlyBrewCount() {
        return persistenceService.getMonthlyBrewCount();
    }

    public int getDailyBrewCount() {
        return persistenceService.getDailyBrewCount();
    }

    public int getDailyGramsBrewed() {
        return persistenceService.getDailyBrewCount();
    }

    public double getAverageBrewingTime() {
        return persistenceService.getAverageBrewingTime();
    }

    public int getAllTimeBrewCount() {
        return persistenceService.getAllTimeBrewCount();
    }

    public int getLast15MinutesBrewCount() {
        return persistenceService.getLast15MinutesBrewCount();
    }
}
