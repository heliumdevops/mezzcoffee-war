package com.mezzanine.mezzcoffee.service;

import com.mezzanine.mezzcoffee.service.persistence.PersistenceService;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 *
 * @author Torbjørn Kristoffersen <tk@mezzanineware.com>
 */
@Singleton
@Startup
public class StartupServiceBean {
    
    @Inject
    private PersistenceService persistenceService;
    
    @PostConstruct
    public void init() {
        persistenceService.updateSchema();
    }
}