package com.mezzanine.mezzcoffee.service.persistence;

import com.mezzanine.mezzcoffee.Util.BrewStatus;
import java.util.Date;

/**
 *
 * @author Torbjørn Kristoffersen <tk@mezzanineware.com>
 */
public interface PersistenceService {

    void updateSchema();

    void insertCoffeeLogEvent(BrewStatus brewStatus, Date tstamp);
    
    double getAverageBrewingTime();
    
    int getMonthlyBrewCount();
    
    int getDailyBrewCount();
    
    int getAllTimeBrewCount();
    
    int getLast15MinutesBrewCount();
}
