package com.mezzanine.mezzcoffee.rest;

import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mezzanine.mezzcoffee.Util.BrewStatus;
import com.mezzanine.mezzcoffee.service.CoffeeServiceBean;
import com.mezzanine.mezzcoffee.service.SlackServiceBean;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;

/**
 * REST Web Service for the MezzCoffee project
 *
 * @author Torbjørn Kristoffersen <tk@mezzanineware.com>
 */
@Path("coffee")
@RequestScoped
public class CoffeeResource {

    private static final int IDLE_THRESHOLD_SECONDS = 3600;

    private static final String FULL_OF_FRESH_COFFEE = "Full of fresh coffee!";

    @Inject
    private CoffeeServiceBean coffeeService;

    @Inject
    private SlackServiceBean slackServiceBean;

    /**
     * If ready state has been on for too long, machine is probably empty (idle)
     *
     * @param lastUpdated
     * @return
     */
    private boolean isProbablyIdle(final Date lastUpdated) {
        final Date now = new Date();
        long diff = (now.getTime() / 1000L) - (lastUpdated.getTime() / 1000L);
        return diff > IDLE_THRESHOLD_SECONDS;
    }

    /**
     * @return the status, whether idle or brewing
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("status")
    public String getStatus() {

        if (coffeeService.getBrewingStatus().equals(BrewStatus.READY.name())
                && isProbablyIdle(coffeeService.getLastUpdated())) {
            coffeeService.setBrewingStatus(BrewStatus.IDLE);
        }

        String brewingStatus = coffeeService.getBrewingStatus();
        if (brewingStatus.equals(BrewStatus.READY.name())) {
            brewingStatus = FULL_OF_FRESH_COFFEE;
        }

        return "{\"status\":\"" + brewingStatus + "\"}";
    }

    /**
     * Coffee machine connects here to notify that coffee pot is ready
     *
     * @return 200
     */
    @GET
    @Path("ready")
    public Response coffeeIsReady() {
        coffeeService.setBrewingStatus(BrewStatus.READY);
        return Response.ok().build();
    }

    /**
     * Coffee machine connects here to notify that coffee pot is ready
     *
     * @return 200
     */
    @GET
    @Path("brewing")
    public Response coffeeIsBrewing() {
        coffeeService.setBrewingStatus(BrewStatus.BREWING);
        return Response.ok().build();
    }

    @GET
    @Path("avgbrewtime")
    public String getAverageBrewingTime() {
        return "{\"value\":" + coffeeService.getAverageBrewingTime() + "}";
    }

    @GET
    @Path("dailyBrewCount")
    public String getDailyBrewCount() {
        return "{\"value\":" + coffeeService.getDailyBrewCount() + "}";
    }

    @GET
    @Path("monthlyBrewCount")
    public String getMonthlyBrewCount() {
        return "{\"value\":" + coffeeService.getMonthlyBrewCount() + "}";
    }

    @GET
    @Path("allTimeBrewCount")
    public String getAllTimeBrewCount() {
        return "{\"value\":" + coffeeService.getAllTimeBrewCount() + "}";
    }

    @GET
    @Path("last15MinutesBrewCount")
    public String getlast15MinutesBrewCount() {
        return "{\"value\":" + coffeeService.getLast15MinutesBrewCount() + "}";
    }

    @POST
    @Path("messageCoffeeRoom/{msg}")
    public Response sendMessageToCoffeeRoom(final @PathParam("msg") String msg) {
        slackServiceBean.sendMessage(msg);
        return Response.ok().build();
    }
}
