create-jdbc-connection-pool --restype="javax.sql.DataSource" --datasourceclassname="org.postgresql.ds.PGSimpleDataSource" --property User=mezzcoffee:DatabaseName=mezzcoffee:LogLevel=0:Password=123456:ServerName=localhost:ProtocolVersion=0:TcpKeepAlive=false:SocketTimeout=0:PortNumber=5432:LoginTimeout=0:UnknownLength=2147483647:PrepareThreshold=5 MezzCoffeePool
create-jdbc-resource --connectionpoolid MezzCoffeePool jdbc/MezzCoffee
create-virtual-server --hosts coffee.mezzanineware.com --defaultwebmodule MezzCoffee-war-0.3 coffee
#Manually edit the config to set the network listener